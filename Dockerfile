FROM node:alpine

COPY src/pylintparser /tmp/pylintParser

# Install Python
RUN apk add --no-cache \
  python3-dev \
  py3-pip && \
  ln -s /usr/bin/python3 /usr/bin/python && \
  ln -s /usr/bin/pip3 /usr/bin/pip

# Install pip packages
RUN apk add --no-cache \
  gcc \
  libffi-dev \
  musl-dev \
  openssl-dev && \
  pip install \
  twine \
  wheel

# Build Pylint Parser
RUN cd /tmp/pylintParser && \
  npm install && \
  npm run build && \
  cp /tmp/pylintParser/bin/pylintparse /bin && \
  rm -rf /tmp/pylintParser
