// http://pylint.pycqa.org/en/latest/user_guide/output.html#pylint-output
export interface PylintFinding {
  "type": PylintTypes
  "module": string
  "obj": string
  "line": number
  "column": number
  "path": string
  "symbol": string
  "message": string
  "message-id": string
}

// http://pylint.pycqa.org/en/latest/user_guide/output.html#source-code-analysis-section
enum PylintTypes {
  Informational = "informational",
  Refactor = "Refactor",
  Convention = "Convention",
  Warning = "Warning",
  Error = "Error",
  Fatal = "Fatal"
}


// https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool
export interface  GitlabFinding {
  description: string
  fingerprint: string
  location: {
    path: string
    lines: {
      begin: number
    }
  }
}