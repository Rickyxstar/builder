import process from "process"
import { PylintFinding, GitlabFinding } from "./types/pylintparse"

const parsePylint = (pylintJson: Event): void  => {
  // Parse Pylint
  const findings: PylintFinding[] = JSON.parse(pylintJson.toString())

  // Create Gitlab finding collection
  const gitlabFindings: GitlabFinding[] = findings.map(finding => ({
    description: finding["message-id"] + ": " +finding.message +" (" + finding.symbol + ")",
    fingerprint: finding["message-id"],
    location: {
      path: finding.path,
      lines: {
        begin: finding.line
      }
    }
  }))

  // Spit out JSON
  process.stdout.write(JSON.stringify(gitlabFindings))
}

process.stdin.on("data", parsePylint)
